import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import User from '../views/User/index.vue'
import uploadFile from '../views/User/uploadFile.vue'
import { getUserData, hasValidToken } from '@/services/auth.service'
import { useUserStore } from '@/store'
import manageStudent from '@/views/User/manageStudent.vue'
import Imprint from '@/views/Imprint.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/user',
      name: 'user',
      component: User
    },
    {
      path: '/user/upload',
      name: 'upload',
      component: uploadFile
    },
    {
      path: '/user/manage',
      name: 'manage',
      component: manageStudent
    },
    {
      path: '/imprint',
      name: 'imprint',
      component: Imprint
    }
  ]
})
//Prevent user from accessing pages that require authentication
router.beforeEach(async (to, from) => {
  const store = useUserStore()
  if (to.name !== 'home' && to.name !== 'imprint' && !hasValidToken()) {
    return { name: 'home' }
  } else {
    const userData = getUserData()
    if (userData) store.setUserAction(userData)
  }
  if (to.name === 'home' && hasValidToken()) {
    return { name: 'user' }
  }
})
export default router
