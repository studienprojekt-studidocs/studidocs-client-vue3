export class ReleaseRequest {
  studentAccountId: string
  documentId: string
  id: string
  releaseState: number
  message: string
  document: Document | undefined

  constructor(
    studentAccountId: string,
    documentId: string,
    id: string,
    releaseState: number,
    message: string,
    document?: Document,
  ) {
    this.studentAccountId = studentAccountId
    this.documentId = documentId
    this.id = id
    this.releaseState = releaseState
    this.message = message
    this.document = document
  }
}
