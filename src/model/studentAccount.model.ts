import { ReleaseRequest } from './releaseRequest.model'
import { UserLogin } from './userLogin.model'

export class StudentAccount {
  expiryDate: string
  docentId: string
  studentId: string
  id: string
  releaseRequests: ReleaseRequest[]
  authWithUserLogin: UserLogin

  constructor(
    expiryDate: string,
    docentId: string,
    studentId: string,
    id: string,
    releaseRequests: ReleaseRequest[],
    authWithUserLogin: UserLogin,
  ) {
    this.expiryDate = expiryDate
    this.docentId = docentId
    this.studentId = studentId
    this.id = id
    this.releaseRequests = releaseRequests
    this.authWithUserLogin = authWithUserLogin
  }
}
