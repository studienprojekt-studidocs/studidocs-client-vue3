export class SearchWord {
  word: string
  id: string

  constructor(word: string, id: string) {
    this.word = word
    this.id = id
  }
}
