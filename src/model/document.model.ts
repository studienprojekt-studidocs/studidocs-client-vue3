import { ReleaseRequest } from './releaseRequest.model'

export class Document {
  title: string
  language: string
  type: string
  year: number
  course: string
  docent: string
  company: string
  restricted: boolean
  abstract: string
  userId: string
  fileName: string
  id: string
  student: string
  releaseRequest: ReleaseRequest | undefined //TODO delete undefined (it is set because of test data)
  private: boolean
  privacyState: number
  searchWords: string[]

  constructor(
    title: string,
    language: string,
    type: string,
    year: number,
    course: string,
    docent: string,
    company: string,
    restricted: boolean,
    abstract: string,
    userId: string,
    fileName: string,
    id: string,
    student: string,
    releaseRequest: ReleaseRequest | undefined,
    privateInput: boolean,
    privacyState: number,
    searchWords: string[],
  ) {
    this.title = title
    this.language = language
    this.type = type
    this.year = year
    this.course = course
    this.docent = docent
    this.company = company
    this.restricted = restricted
    this.abstract = abstract
    this.userId = userId
    this.fileName = fileName
    this.id = id
    this.student = student
    this.releaseRequest = releaseRequest //TODO delete parameter type undefined (it is set because of test data)
    this.private = privateInput
    this.privacyState = privacyState
    this.searchWords = searchWords
  }
}
