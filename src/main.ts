import './assets/style.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import PrimeVue from 'primevue/config'
import Ripple from 'primevue/ripple'

import App from './App.vue'
import router from './router'
import { useUserStore } from './store'

const app = createApp(App)
app.directive('ripple', Ripple)

app.use(createPinia())
app.use(router)
useUserStore()

app.use(PrimeVue, {
  unstyled: true,
  ripple: true,
  pt: {
    chips: {
      root: ({ props: e }) => ({
        class: ['flex', { 'opacity-60 select-none pointer-events-none cursor-default': e.disabled }]
      }),
      container: ({ state: e }) => ({
        class: [
          'font-sans text-lg leading-none',
          'flex items-center flex-wrap gap-1',
          'w-full',
          'list-none',
          'rounded',
          'bg-[#F0F3F5]',
          'placeholder:studidocs_red',
          {
            'ring-1 ring-inset ring-surface-300 ring-offset-0': !e.focused,
            'ring-2 ring-primary-500': e.focused
          },
          'cursor-text overflow-hidden',
          'appearance-none'
        ]
      }),
      inputtoken: {
        class: ['px-0', 'inline-flex flex-auto']
      },
      input: {
        class: [
          'px-2 rounded bg-[#F4F4F5] text-studidocs_primary_text placeholder-studidocs_red p-2 font-semibold w-full caret-studidocs_red outline-none'
        ]
      },
      token: {
        class: [
          'inline-flex items-center',
          'py-0.5 px-3',
          'rounded',
          'text-studidocs_primary_text',
          'font-semibold',
          'bg-white'
        ]
      },
      label: {
        class: 'leading-5'
      },
      removeTokenIcon: {
        class: [
          'rounded-md leading-6',
          'ml-2',
          'w-4 h-4',
          'transition duration-200 ease-in-out',
          'cursor-pointer'
        ]
      }
    }
  }
})

app.mount('#app')
