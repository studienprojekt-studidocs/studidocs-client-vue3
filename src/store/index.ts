import { defineStore } from 'pinia'
import { setCookie, getCookie } from '@/utils/cookie.utils'
import { getDocuments, getOwnDocuments } from '@/services/api/documents.service'
import { getStudentAccounts } from '@/services/api/studentAccounts.service'

export interface User {
  userName: string
  userToken: string | undefined
  userRole: string
  userId: string
}

interface CookieState {
  allowed: boolean
  info: boolean
}

export const useUserStore = defineStore('mainStore', {
  state: (): {
    user: User
    cookieState: CookieState
    documents: any[]
    ownDocuments: any[]
    studentAccounts: any[]
    searchWords: any[]
  } => ({
    user: {
      userName: '',
      userToken: undefined,
      userRole: 'commonUser',
      userId: ''
    },
    cookieState: { allowed: false, info: true },
    documents: [],
    ownDocuments: [],
    studentAccounts: [],
    searchWords: []
  }),
  getters: {
    getUserName: (state) => state.user.userName,
    getUserToken: (state) => state.user.userToken,
    getUserId: (state) => state.user.userId,
    getUserRole: (state) => state.user.userRole,
    getCookiesAllowed: (state) => state.cookieState.allowed,
    getCookiesInfoState: (state) => state.cookieState.info
  },
  actions: {
    setUserAction(data: User) {
      this.user = data
    },
    resetUserAction() {
      this.user = { userName: '', userToken: undefined, userRole: 'commonUser', userId: '' }
    },
    async initCookieState() {
      if (getCookie('cookiesOn')) {
        this.cookieState = { allowed: true, info: false }
      } else {
        this.cookieState = { allowed: false, info: true }
      }
    },
    setCookiesState(data: { cookies: boolean }) {
      this.cookieState = { allowed: data.cookies, info: false }
      if (data.cookies) {
        setCookie('cookiesOn', 'true')
      }
    },
    async getDocumentsState(skipFilter: number) {
      this.documents = await getDocuments(skipFilter)
    },
    async getOwnDocumentsState() {
      this.ownDocuments = await getOwnDocuments()
    },
    async getStudentAccountsState() {
      this.studentAccounts = await getStudentAccounts()
    }
  }
})
