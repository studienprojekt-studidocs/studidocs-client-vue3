import { Route } from 'vue-router'
import { isNull, isEqual } from 'lodash'
import store from '@/store'

/**
 * Checks if two routes are the same.
 * Ignores leading or trailing slashes.
 *
 * @param r1 - One route
 * @param r2 - Another route
 * @param topRouteOnly - Compare only the first part of the routes? See examples below.
 * @example
 *
 * isSameRoute('/abc', '/abc')
 * // -> true
 *
 * isSameRoute('/abc', '/xyz')
 * // -> false
 *
 * isSameRoute('/abc', '/abc/') && isSameRoute('/abc', 'abc')
 * // -> true
 *
 * isSameRoute('/abc', '/abc/def')
 * // -> false
 *
 * isSameRoute('/abc', '/abc/def', true)
 * // -> true
 */
export function isSameRoute(
  r1: Route | string,
  r2: Route | string,
  topRouteOnly: boolean = false,
): boolean {
  let routes1 = ((r1 as any).path || r1).match(/\w+/)
  let routes2 = ((r2 as any).path || r2).match(/\w+/)

  return topRouteOnly
    ? routes1 !== null && routes2 !== null && routes1[0] === routes2[0]
    : isEqual(routes1, routes2)
}

export function hasUserRequiredRole(requiredRoles: string[]): boolean {
  if (
    requiredRoles.length == 0 ||
    requiredRoles.includes(store.state.user.userRole)
  ) {
    return true
  }
  return false
}
