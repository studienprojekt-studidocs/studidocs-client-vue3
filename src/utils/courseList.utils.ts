const courseList: string[] = [
  'Bank',
  'Bauingenieurwesen',
  'Dienstleistungsmanagement',
  'Industrielle Elektrotechnik',
  'Technisches Facility Management',
  'Handel',
  'IBA',
  'Immobilienwirtschaft',
  'Industrie',
  'Informatik',
  'Maschinenbau',
  'PPM',
  'Spedition und Logistik',
  'Steuern und Prüfungswesen',
  'Tourismus',
  'Versicherung',
  'Wirtschaftsinformatik',
]

export function getCourseList() {
  return courseList
}
