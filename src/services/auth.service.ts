import _ from 'lodash'
import dateFns from 'date-fns'
import Axios from 'axios'
import { baseUrl } from '@/utils/http.utils'
import { useUserStore, type User } from '@/store'

// key for userToken in sessionstorage
const userTokenKey = 'userToken'
const userDataKey = 'userData'
/**
 * Checks if there is a token entry in the sessionStorage, otherwise the stored user data gets deleted.
 */
export function isTokenStillSet(): boolean {
  let token = sessionStorage.getItem(userTokenKey) || ''
  if (token != '') {
    return true
  }
  return false
}

/**
 * Saves a token entry in the sessionStorage.
 * If there already is a token, the newer token overwrites the older token.
 */
function saveToken(token: string): void {
  const useStore = useUserStore()

  if (useStore.cookieState.allowed) sessionStorage.setItem(userTokenKey, token)
}

function saveUserData(data: User): void {
  const useStore = useUserStore()

  if (useStore.cookieState.allowed) sessionStorage.setItem(userDataKey, JSON.stringify(data))
}

/**
 * Deletes the token entry from sessionStorage.
 */
function deleteToken(): void {
  sessionStorage.removeItem(userTokenKey)
}

function deleteUserData(): void {
  sessionStorage.removeItem(userDataKey)
}

/**
 * Decodes a JSON Web Token (token entry) and returns it's payload as JSON object
 * @param token The token entry as string
 */
function decodeToken(token: string) {
  return JSON.parse(atob(token))
}

/**
 * Checks the token, if it is valid. When validation is possitiv it decodes the token entry and returns it's payload as JSON object
 * @returns object if token is valid
 */
export function getDecodedToken() {
  if (hasValidToken()) {
    let token = sessionStorage.getItem(userTokenKey) || ''
    return decodeToken(token)
  }
  return null
}

/**
 * Checks if a token entry is expired.
 * @returns true if the token's expiration date is exceeded or if the token has no expiration date.
 */
function isTokenExpired(token: string): boolean {
  let decoded = decodeToken(token)
  let hasExpProperty = decoded.hasOwnProperty('exp') //the 'exp' property useStores the expiration date of the token
  if (!hasExpProperty) {
    //a valid token must have a 'exp' property
    return true
  }
  let expDate = dateFns.parse(decoded.exp)
  return !dateFns.isAfter(expDate, new Date())
}

/**
 * Checks if there is a token entry in the sessionStorage, which is not expired.
 */
export function hasValidToken(): boolean {
  let token = sessionStorage.getItem(userTokenKey) || ''
  return !_.isEmpty(token) && !isTokenExpired(token)
}

export function hasUserData(): User | null {
  let data = sessionStorage.getItem(userDataKey) || ''
  if (data != '') {
    return JSON.parse(data)
  }
  return null
}

/**
 * Retrieves a the accessToken from sessionStorage.
 */
export function getAccessToken() {
  let token = getDecodedToken()
  if (token) {
    return token.accessToken
  } else {
    return null
  }
}

export function getUserData() {
  let data = hasUserData()
  if (data) {
    return data
  } else {
    return null
  }
}

/**
 * Sends username and password to the server.
 * If the credentials are valid, the server sends a token entry. This token will be saved into the sessionStorage.
 */
export async function login(username: string, password: string): Promise<void> {
  const data = {
    username: username,
    password: password
  }

  const requestConfig = {
    method: 'post',
    url: `${baseUrl}/userLogins/login`,
    data: data,
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' }
  }

  return new Promise((resolve, reject) => {
    const useStore = useUserStore()

    Axios(requestConfig)
      .then((response) => {
        //success
        let token: {
          accessToken: string
          exp: Date
          userName: string
          userRole: string
          userId: string
        } = {
          accessToken: response.data.token,
          exp: dateFns.addSeconds(dateFns.parse(dateFns.endOfToday()), 0), //change time if not correct //TODO Franzi
          userName: response.data.userName,
          userRole: response.data.userRole,
          userId: response.data.userId
        }
        saveToken(btoa(JSON.stringify(token)))
        const userData: User = {
          userName: token.userName,
          userToken: token.accessToken,
          userRole: token.userRole,
          userId: token.userId
        }
        saveUserData(userData)
        useStore.setUserAction(userData)
        resolve()
      })
      .catch((error) => {
        reject(error.response.data.error)
      })
  })
}

/**
 * Logs out the current user.
 * Deletes the token entry from sessionStorage.
 */
export function logout(): void {
  const useStore = useUserStore()
  useStore.resetUserAction()
  deleteToken()
  deleteUserData()
}

export default {
  getAccessToken,
  isTokenStillSet,
  getDecodedToken,
  login,
  logout,
  hasValidToken
}
