import { Document } from '@/model/document.model'
import Axios from 'axios'
import { baseUrl } from '@/utils/http.utils'
import { getAccessToken } from '../auth.service'
import { useUserStore } from '@/store'

// parse a document or an array of documents
const parseDocument = (s: any) =>
  new Document(
    s.title,
    s.language,
    s.type,
    s.year,
    s.course,
    s.docent,
    s.company,
    s.restricted,
    s.abstract,
    s.userId,
    s.fileName,
    s.id,
    s.student,
    s.releaseRequests,
    s.private,
    s.privacyState,
    s.searchWords
  )
function parseDocuments(sy: any): Document[] {
  if (Array.isArray(sy)) {
    return sy.map((s: any) => parseDocument(s))
  } else {
    return [parseDocument(sy)]
  }
}

/**
 * Retrieves all documents.
 */
export function getDocuments(skipFilter: number): Promise<Document[]> {
  if (skipFilter == undefined) {
    skipFilter = 0
  }
  const requestConfig = {
    method: 'get',
    url: `${baseUrl}/documents/documentInfo?filter[skip]=${skipFilter}`,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      /* ...( getAccessToken() && {'accessToken': store.getters.getUserToken}) */
      accessToken: getAccessToken()
    }
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response: { data: { results: any } }) => {
        resolve(
          parseDocuments(
            Object.assign(response.data.results, {
              releaseRequests: undefined
            })
          )
        )
      })
      .catch((error: { response: any }) => {
        reject(error.response)
      })
  })
}

/**
 * Retrieves all documents.
 */
export function getOwnDocuments(): Promise<Document[]> {
  const useStore = useUserStore()
  const requestConfig = {
    method: 'get',
    url: `${baseUrl}/documents/ownDocumentInfo`,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...(getAccessToken() && { accessToken: useStore.getUserToken })
    }
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(parseDocuments(response.data))
      })
      .catch((error) => {
        reject(error)
      })
  })
}

/**
 * Retrieves pdf file to document entry.
 */
export function getDocumentPDF(container: string, filename: string): Promise<string> {
  const useStore = useUserStore()

  const requestConfig = {
    method: 'post',
    url: `${baseUrl}/documents/download`,
    headers: {
      'Content-Type': 'application/json',
      ...(getAccessToken() && { accessToken: useStore.getUserToken })
    },
    data: {
      container: container,
      file: filename
    },
    responseType: 'blob'
  }
  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]))
        resolve(url)
      })
      .catch((error) => {
        reject(error.response)
      })
  })
}

export function getFile(container: string, filename: string): Promise<any> {
  const useStore = useUserStore()

  const requestConfig = {
    method: 'post',
    url: `${baseUrl}/documents/download`,
    headers: {
      'Content-Type': 'application/json',
      ...(getAccessToken() && { accessToken: useStore.getUserToken })
    },
    data: {
      container: container,
      file: filename
    },
    responseType: 'arraybuffer'
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error.response)
      })
  })
}

export function filterDocuments(
  data: { language: string; year: number; type: string },
  skipFilter: number
): Promise<Document[]> {
  const useStore = useUserStore()

  var filter = mapFilterData(data)
  const requestConfig = {
    method: 'get',
    url: `${baseUrl}/documents/filterDocuments?filter=${JSON.stringify(
      filter
    )}&filter[skip]=${skipFilter}`,
    headers: {
      'Content-Type': 'application/json',
      ...(getAccessToken() && { accessToken: useStore.getUserToken })
    }
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(parseDocuments(response.data))
      })
      .catch((error) => {
        reject(error.response)
      })
  })
}

export function getDocumentCount(): Promise<number> {
  const useStore = useUserStore()

  const requestConfig = {
    method: 'get',
    url: `${baseUrl}/documents/getDocumentCount`,
    headers: {
      'Content-Type': 'application/json',
      ...(getAccessToken() && { accessToken: useStore.getUserToken })
    }
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error)
      })
  })
}

function mapFilterData(data: any): any {
  var filter = {
    where: {
      and: [{}]
    }
  }
  var and = []
  if (data.language) and.push({ language: data.language })
  if (data.year) and.push({ year: '' + data.year })
  if (data.type) and.push({ type: data.type })
  filter.where.and = and
  return filter
}

/**
 * Add all document.
 */
export function addDocument(data: {
  title: string
  language: string
  type: string
  year: number
  course: string
  docent: string
  company: string
  restricted: boolean
  abstract: string
  student: string
  privacyState: number
  searchWords: string[]
  file: Blob
}): Promise<Document> {
  const useStore = useUserStore()
  console.log('Token', useStore.getUserToken)

  var bodyFormData = new FormData()
  bodyFormData.set('title', data.title)
  bodyFormData.set('language', data.language)
  bodyFormData.set('type', data.type)
  bodyFormData.set('year', String(data.year))
  bodyFormData.set('course', data.course)
  bodyFormData.set('docent', data.docent)
  bodyFormData.set('company', data.company)
  bodyFormData.set('restricted', String(data.restricted))
  bodyFormData.set('abstract', data.abstract)
  bodyFormData.set('student', data.student)
  bodyFormData.set('privacyState', String(data.privacyState))
  bodyFormData.set('searchWords', JSON.stringify(data.searchWords))
  bodyFormData.append('file', data.file)
  const requestConfig = {
    method: 'post',
    url: `${baseUrl}/documents/upload`,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      ...(getAccessToken() && { accessToken: useStore.getUserToken })
    },
    data: bodyFormData
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error.response)
      })
  })
}

/**
 * Update document.
 */
export function updateDocument(data: {
  id: string
  title: string
  language: string
  type: string
  year: number
  course: string
  docent: string
  company: string
  restricted: boolean
  abstract: string
  student: string
  privacyState: number
  searchWords: string[]
  file: Blob
}): Promise<Document> {
  const useStore = useUserStore()

  var bodyFormData = new FormData()
  bodyFormData.set('id', data.id)
  bodyFormData.set('title', data.title)
  bodyFormData.set('language', data.language)
  bodyFormData.set('type', data.type)
  bodyFormData.set('year', String(data.year))
  bodyFormData.set('course', data.course)
  bodyFormData.set('docent', data.docent)
  bodyFormData.set('company', data.company)
  bodyFormData.set('restricted', String(data.restricted))
  bodyFormData.set('abstract', data.abstract)
  bodyFormData.set('student', data.student)
  bodyFormData.set('privacyState', String(data.privacyState))
  bodyFormData.set('searchWords', JSON.stringify(data.searchWords))
  bodyFormData.append('file', data.file)
  const requestConfig = {
    method: 'put',
    url: `${baseUrl}/documents/updateDocument`,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      ...(getAccessToken() && { accessToken: useStore.getUserToken })
    },
    data: bodyFormData
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error.response)
      })
  })
}

export function deleteDocument(data: { id: string }) {
  const useStore = useUserStore()

  var bodyFormData = new FormData()
  bodyFormData.set('id', data.id)
  const requestConfig = {
    method: 'delete',
    url: `${baseUrl}/documents/deleteDocument`,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      ...(getAccessToken() && { accessToken: useStore.getUserToken })
    },
    data: bodyFormData
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error.response)
      })
  })
}
