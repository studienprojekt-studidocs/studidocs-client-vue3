/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        studidocs_red: '#F75056',
        studidocs_red_hover: '#c33f44',
        studidocs_light_grey: '#F0F3F5',
        studidocs_light_grey_hover: '#bec0c2',
        studidocs_primary_text: '#404040',
        studidocs_secondary_text: '#646973'
      }
    }
  },
  plugins: []
}
