# studidocs-client-vue3

studidocs client vue3 is the vue3 Version of the studidocs Frontend. Using a new UI and several newer Versions of dependencies e.g. pinia for State Management.

The Application is a Vue SPA. Being familiar with Vue and Vue Router makes the life easier.

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
   1. Run `Extensions: Show Built-in Extensions` from VSCode's command palette
   2. Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

## Log in into App

### Setup

When you want to log in into the app, make sure that the Backend is running. Also make sure that in `src/utils/http.utils.ts` you have selected the right Port for the `baseUrl` constant.

### Credentials

You can either log yourself in with your own HWR credentials as you know it from Moodle or S.A.M. If you want to log in as a user with sudo rights use **Testaccount** as the username. The password can be anything.

## Roles

### Student

Student is the base role for person that is not a docent. Every Student is abel to Log in into the Application with their HWR Credentials like `s_mustermann21`. A studnet with basic rights is only able to view documents of other students.

Every student can get extendet rights to be able to upload or delete own documents. These rights are given by a docent. Although a student is given the right to upload a document every single file has to be approved by the docent who gave access. 

### Docent

Basically, the only task of the docent role is that the docent allows students to upload their work to the platform. Why a docent has to allow a student to upload their own work is not yet clear to us. The need for this is unfortunately not documented anywhere.

## Styling

For styling components we are using the PrimeVue UI Library in combination with Tailwind CSS

### PrimeVue

We are using unstyled [PrimeVue](https://tailwind.primevue.org/) UI Components for maximal flexibility. Style the Components with PassTroughs and TailwindCSS classes.

### TailwindCSS

[TailwindCSS](https://tailwindcss.com/) is a great tool for styling your components faster. With the use of Tailwind we do not need the `style` Tag. But there are alway some special cases.

## State Management

For the State Management we use Pinia a store for Vue. Many states are migrated from the old Vue2 Frontend. You can use ste states of our store in your Component by importing the store and then declaring a variable. Like this:

```vue
import { useUserStore } from './relative/path/to/store'

const store = useUserStore()
```
You find the index file for state management under `src/store/index.ts`. 
Now you can access all states via the `store` const in your Component.

For further documentation and how to use Pinia please have a look at their [Docs](https://pinia.vuejs.org/) 🍍

## Directories

The Vue3 got several directories that are copied from the Vue2 Frontend such as the `src/services` here you find basically all functions that deal with the API/Backend.

The `src/utils` deals with e.g. cookies that we need to store user data.

The `src/model` Directory holds several Classes that are useful since we are using TypeScript.

### views

Every single view should have its own route in the `router/index.ts`. From the User perspective every view is seen as a several page.

### components

Every view consists of multiple Components from the `components` directory. We should make sure, that we can use a single Component as often as we can. All Components that serve a specific purpose can be be grouped in a subdirectory.

## automated Tests

as of now there are no automated Tests such as Unit or E2E Tests.

## Other useful Information

When you are logged in as a student you might not have the right to access or upload any files. The right to do that have to be given by a docent. Therefore the Application should contain an Admin area where only a docent get access.

FYI: The **Testaccount** as explained above has the role docent.

If you make any significant changes to the code that are useful for future maintainers to know, please be sure to document them in this README.
Please make sure to do the same thing when introducing a new plugin such as i18n. btw we should do that 🙂.

For everyone wo works on this Project after us just request access to the Project/group we hope that we get a notification...

### UI Design

You can find the redesigned UI Design in these [Sketch Files](https://www.sketch.com/s/f127c4c4-e35f-423f-a096-77ea4caf026b). Not every view is covered but it should give you an idea how to build components.
